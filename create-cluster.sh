#!/bin/bash

if [ $# -lt 1 ]; then
   echo "Error: cluster name required."
   echo "Usage: $(basename $0) <cluster name> [region]"
   exit 1
else
   CLUSTER="$1"
   if [ -n "$2" ]; then
      REGION="$2"
   else
      REGION="`gcloud config list 2>/dev/null | awk -F"\ =\ " '/^region/{print $2}'`"
   fi
fi

gcloud container clusters create $CLUSTER --num-nodes=3 --machine-type=n1-standard-2 --region="$REGION" 
