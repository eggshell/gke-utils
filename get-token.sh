#!/bin/bash

if [ $# -lt 4 ]; then
    echo "Error: cluster name and token name required."
    echo "Usage: $(basename $0) -c <cluster name> -t <token name>"
    exit 1
else
    while getopts "c:t:" arg; do
        case ${arg} in
            c)
                CLUSTER_SHORT=${OPTARG}
                ;;
            t)
                TOKEN_NAME=${OPTARG}
                ;;
            --)
                break;;
            *)
                echo "Usage: $(basename $0) -c <cluster name> -t <token name>"
                exit 1
                ;;
        esac
    done
fi

CLUSTER_FULL="`kubectl config get-contexts | grep "$CLUSTER_SHORT" | awk '{if ($1 != "*") {print $2} else {print $3}}'`"

# Retrieve the token for the service account
echo "SERVICE ACCOUNT TOKEN ="
TOKEN="$(kubectl --cluster="$CLUSTER_FULL" -n kube-system get secrets | grep "${TOKEN_NAME}" | cut -d' ' -f1)"
kubectl --cluster="$CLUSTER" -n kube-system get secret $TOKEN -o jsonpath="{['data']['token']}" | base64 --decode
echo ""
