#!/bin/bash

if [ $# -lt 1 ]; then
   echo "Error: cluster name required."
   echo "Usage: $(basename $0) <cluster name>"
   exit 1
else
   CLUSTER="$1"
   CLUSTER_FULL="`kubectl config get-contexts | grep "$CLUSTER" | awk '{if ($1 != "*") {print $2} else {print $3}}'`"

   ENDPOINT="`kubectl --cluster="$CLUSTER_FULL" cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'`"

   echo "API ENDPOINT URL= "
   if [ -n "$ENDPOINT" ]; then
      echo $ENDPOINT
   else
      echo "Error: No endpoint found matching this cluster?"
      exit 1
   fi
fi
