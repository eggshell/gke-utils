#!/bin/bash

if [ "$#" -lt 1 ]; then
   echo "Usage: `basename $0` <cluster name>"
   exit 1
else
   CLUSTER="$1"
fi

# Get service and delete associated load ballancer
#  kubectl delete service $SERVICE

# Delete cluster
# gcloud container clusters delete $CLUSTER
